import React, { Component } from 'react';
// Task component - represents a single todo item
export default class Coin extends Component {
  render() {
    return (

      <tr className="border-left-0 border-bottom-1">
      	<td>{this.props.coin._id}</td>
      	<td className="border">
      		{this.props.coin.coin} 
      		<span className="p-2 text-black-50">{this.props.coin.symbol}</span>
      	</td>
      	<td className="border">
      		<span className="p-2 border border-info">{this.props.coin.price}</span>
      	</td>
      </tr>
    );
  }

}