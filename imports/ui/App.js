import React, { Component } from 'react';
 
import Coin from './Coinlist.js';
import '../../public/bootstrap-4.1.3/css/bootstrap.min.css';
import '../../public/bootstrap-4.1.3/css/bootstrap.css';
 
// App component - represents the whole app
export default class App extends Component {
  getCoins() {
    return [
	      { _id: 1, logo: 'bitcoin.png', symbol: 'BTC', coin: 'Bitcoin', price: '$6528.19'},
	      { _id: 2, logo: 'ethereum.png', symbol: 'ETH', coin: 'Ethereum', price: '$221.76' },
	      { _id: 3, logo: 'xrp.png', symbol: 'XRP', coin: 'XRP', price: '$0.43' },
	      { _id: 4, logo: 'bitcoin-cash.png', symbol: 'BCH', coin: 'Bitcoin Cash', price: '$0.45' },
	      { _id: 5, logo: 'eos.png', symbol: 'EOS', coin: 'EOS', price: '$5.69' },
	      { _id: 6, logo: 'stellar.png', symbol: 'XLM', coin: 'Stellar', price: '$0.23' },
	      { _id: 7, logo: 'litecoin.png', symbol: 'LTC', coin: 'Litecoin', price: '$56.99' },
	      { _id: 8, logo: 'tether.png', symbol: 'USDT', coin: 'Tether', price: '$0.99' },
	      { _id: 9, logo: 'cardano.png', symbol: 'ADA', coin: 'cardano', price: '$0.083' },
	      { _id: 10, logo: 'monero.png', symbol: 'XMR', coin: 'Monero', price: '$117.76' },
    ];
  }
 
  renderCoins() {
    return this.getCoins().map((coin) => (
      <Coin key={coin._id} coin={coin} />
    ));
  }
 
  render() {
    return (

      <div className="container">
	    <div className="row">
	    	<div className="col-sm-4"></div>
	        <div className="col-sm-4 border">
	        	<div className="row my-1">
				  <div className="form-group has-search col-sm">
				    <span className="fa fa-search form-control-feedback"></span>
				    <input type="text" class="form-control" placeholder="Search coins" />
				  </div>

		   			<div className="col-sm-4 text-center fs-12 c-8cff1a">Price by Market Cap</div>
		   			<div className="col-xs"><i className='fa fa-sort c-dee2e6'></i></div>

		        </div>
		        <table className="table ">
		        	<thead>
		        		<tr>
			        		<th className="w55">#</th>
			        		<th>Currencies</th>
			        		<th>Price</th>
			        	</tr>
		        	</thead>
		        	<tbody>
		          		{this.renderCoins()}
		          	</tbody>
		        </table>
		    </div>
	    	<div className="col-sm-4"></div>
	    </div>
        
      </div>
    );
  }
}